require 'snapshot/version'

ActiveRecord::Base.extend(Snapshot)

module Snapshot
  def take_snapshot(options)
    raise ArgumentError unless options.is_a?(Hash)
    options.reverse_merge! :at => :save, :in => :snapshot

    raise "There is no column '#{options[:in]}' to save the snapshot!" unless column_names.include?(options[:in].to_s)
    raise ArgumentError unless [:save, :create].include?(options[:at])
    raise ArgumentError unless options[:from].is_a?(Hash)

    serialize :snapshot, type: Hash, coder: YAML

    case options[:at]
      when :save
        before_save :store_snapshot_data
      when :create
        before_create :store_snapshot_data
    end

    class_attribute :snapshot_options
    self.snapshot_options = options

    snapshot_options[:from].each_pair do |assoc, fields|
      fields.each do |field|
        define_method "snapshot_#{assoc}_#{field}" do
          (snapshot||{})[assoc.to_sym].try(:[], field.to_sym)
        end
      end
    end

    include InstanceMethods
  end

  module InstanceMethods
    def store_snapshot_data
      snapshot = {}
      snapshot_options[:from].each_pair do |assoc, fields|
        if record = self.send(assoc)
          snapshot[assoc.to_sym] = {}
          fields.each do |field|
            snapshot[assoc.to_sym][field.to_sym] = record.send(field)
          end
        end
      end

      self.send("#{snapshot_options[:in]}=", snapshot)
    end
  end
end
