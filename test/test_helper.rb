require 'minitest/autorun'
require 'active_record'
require 'snapshot'

ActiveRecord::Base.establish_connection(:adapter => "sqlite3", :database => ":memory:")

ActiveRecord::Schema.verbose = false
ActiveRecord::Schema.define(:version => 1) do
  create_table :payments do |t|
    t.integer :ledger_id
    t.string :value
    t.text :snapshot
  end

  create_table :ledgers do |t|
    t.string :number
    t.string :owner
  end
end

puts "Testing with ActiveRecord #{ActiveRecord::VERSION::STRING}"

class Payment < ActiveRecord::Base
  belongs_to :ledger
  take_snapshot :from => { :ledger => [ :number, :owner ] }
end

class Ledger < ActiveRecord::Base
  has_many :payments
end
