require 'test_helper'

class SnapshotTest < Minitest::Test
  def setup
    @ledger = Ledger.create! :number => '42', :owner => 'Mr. Foo'
  end

  def test_save
    payment = @ledger.payments.create! :value => 100

    assert_equal({ :ledger => { :number => '42', :owner => 'Mr. Foo' }}, payment.reload.snapshot)
  end

  def test_skip_snapshot_for_missing_association
    payment = Payment.create! :value => 100
    payment.reload

    assert_equal({}, payment.snapshot)
    assert_nil payment.snapshot_ledger_number
  end

  def test_new_record
    assert_nil Payment.new.snapshot_ledger_number
  end

  def test_adding_methods
    payment = @ledger.payments.create! :value => 100

    assert_equal '42', payment.snapshot_ledger_number
    assert_equal 'Mr. Foo', payment.snapshot_ledger_owner
  end
end
