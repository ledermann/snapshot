# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'snapshot/version'

Gem::Specification.new do |gem|
  gem.name          = "snapshot"
  gem.version       = Snapshot::VERSION
  gem.authors       = ["Georg Ledermann"]
  gem.email         = ["georg@ledermann.dev"]
  gem.description   = %q{Take a snapshot of attributes}
  gem.summary       = %q{Active record extension to store current values of associated records}
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]

  gem.add_dependency 'activerecord'

  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'sqlite3'
  gem.add_development_dependency 'minitest'
end
